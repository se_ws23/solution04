package lnf.report

enum LostItemState {
  case Init, ReportLostItem, ProvideItemDetails, Stored, Transport, Final
}