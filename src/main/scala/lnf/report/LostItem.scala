package lnf.report

import lnf.Item
import lnf.Location.{Stadtmitte, Lichtwiese}
import lnf.report.LostItemState.{Final, Init, ProvideItemDetails, ReportLostItem, Stored, Transport}

class LostItem:
  var item: Option[Item] = None

  private def submit(): Boolean = item.isDefined

  private def stored(): Unit =
    println(raw".________________________.")
    println(raw"||__===____________===__||")
    println(raw"||                      ||")
    println(raw"|:----------------------:|")
    println(raw"|| ${item.get.box(18)} ||")
    println(raw"||______________________||")
    println(raw"''----------------------''")
  
  private def transported(): Unit =
    println(raw"        _______")
    println(raw"       //  ||\ \ ")
    println(raw" _____//___||_\ \___")
    println(raw" )  _ ${item.get.box(6)} _    \ ")
    println(raw" |_/ \________/ \___|")
    println(raw"___\_/________\_/______")

  var currentState: LostItemState = Init

  def transition(): Unit = currentState match
    case Init =>
      currentState = ReportLostItem
    case ReportLostItem =>
      currentState = ProvideItemDetails
    case ProvideItemDetails =>
      val success = submit()
      if (success)
        currentState = Stored
    case Stored =>
      if (item.get.location == Lichtwiese)
        currentState = Transport
      if (item.get.location == Stadtmitte) {
        stored()
        currentState = Final
      }
    case Transport =>
      transported()
      currentState = Final
    case Final =>